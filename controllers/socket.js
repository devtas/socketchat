var express = require('express'),
    app = express(),
    http = require('http').Server(app),
    io = require('socket.io')(http),
    users = {},
    userNumber = 0;

function getUsers() {
  var userNames = [];
  for (var name in users) {
    if (users[name]) {
      userNames.push(name);
    }
  }
  return userNames;
}

io.on('connection', function(socket){
  var myNumber = userNumber++;
  var myName = 'user#' + myNumber;
  users[myName] = socket;

  var foo = getUsers();
  console.info(foo);

  io.emit('hello', { hello: myName });

  io.emit('listing', getUsers());

  socket.on('test', function(val) {
    io.emit('test', val);
  });

  socket.on('message', function(data) {
    io.emit('message', myName + '-> ' + data.message);
  });

  socket.on('disconnect', function() {
    users[myName] = null;
    io.emit('listing', getUsers());
  });
});