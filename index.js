var express = require('express'),
    app = express(),
    http = require('http').Server(app),
    path = require('path'),
    io = require('socket.io')(http),
    routes = require('./controllers/chat'),
    users = {},
    userNumber = 0,
    http_port = 5050;

// Setting folders
app.set('views', path.join(__dirname, 'views'));
app.use(express.static(path.join(__dirname, 'public')));
app.set('view engine', 'jade');
app.use('/', routes);

function getUsers() {
  var userNames = [];
  for (var name in users) {
    if (users[name]) {
      userNames.push(name);
    }
  }
  return userNames;
}

io.sockets.on('connection', function(socket) {
  socket.on('user', function(val) {
    var myName = val.username;
    socket.user = val.username;
    users[myName] = socket.user;

    // Emits
    io.emit('hello', { hello: myName });
    io.emit('listing', getUsers());
    console.info(getUsers());
  });

  socket.on('message', function(data) {
    io.emit('message', socket.user + '-> ' + data.message);
  });

  socket.on('disconnect', function() {
    if (socket.user != undefined) {
      users[socket.user] = null;
      io.emit('listing', getUsers());
    }
  });
});

http.listen(http_port, function(){
  console.log('listening on *:' + http_port);
});

module.exports = app;