var socket = io();
var users;

$('form#username').submit(function(){
  var userName = $('#u').val();
  socket.emit('user', {
    username: userName
  });
  $('.mask').css('display','none');
  $('.content').css('display','block');
  return false;
});

$('form#message').submit(function(){
  var messageValue = $('#m').val();
  socket.emit('message', {
    message: messageValue
  });
  $('#m').val('');
  return false;
});

$('#sair').bind('click', function() {
  socket.emit('disconnect');
});

socket.on('hello', function(data) {
  $('#messages').append($('<li>').text(data.hello + " se conectou!"));
});

socket.on('listing', function(data) {
  users = data;
  console.info("users", users);
  $('#users').html(users.length);
});

socket.on('message', function(message) {
  console.log(message);
  $('#messages').append($('<li>').text(message));
});